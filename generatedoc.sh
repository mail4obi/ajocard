export DOC_SAMPLE_URL="http://localhost:10010"
s4o-mongoose-apidoc -i ./api/models/ -o ./api/controllers/_doc_models.js -e password
s4o-swagger-generator "Ajocard api" ./api/controllers/ ./api/swagger/swagger.yaml
apidoc -i ./api/controllers/ -o ./apidoc/