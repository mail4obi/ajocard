module.exports = {
  // app
  APP_ID: process.env.APP_ID,
  TOKEN_SECRET: process.env.TOKEN_SECRET,
  // redis
  LOCAL_REDIS_SERVER: process.env.LOCAL_REDIS_SERVER,
  EXTERNAL_REDIS_QUEUE: process.env.EXTERNAL_REDIS_QUEUE,
};
