const axios = require('axios');
const { get } = require('lodash');

/**
 * @param {{ URL, TOKEN }} config
 * @param {String} method
 * @param {Any} params
 */
const rpc = async (config, method, params) => {
  try {
    const response = await axios({
      method: 'POST',
      url: config.URL,
      data: {
        jsonrpc: '2.0', id: Date.now(), method, params,
      },
      headers: {
        authorization: `Bearer ${config.TOKEN}`,
      },
    });
    if (response.data.error) {
      console.log(response.data);
      throw new Error(response.data.error.message);
    }
    return response.data.result;
  } catch (error) {
    throw new Error(get(error, 'message', 'unable to get manager response'));
  }
};

module.exports = {
  rpc,
};
