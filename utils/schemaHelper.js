const {
  omit,
  forEach,
  get,
} = require('lodash');

class Schema {
  constructor(defs, options) {
    this.defs = defs;
    this.options = options;
    this.secondaryIndexes = [];
  }

  get definitions() {
    return this.defs;
  }

  path(prop) {
    this.validations = [];
    return { validate: (fn) => { } };
  }

  index(indexes, options) {
    this.secondaryIndexes.push({ indexes, options });
  }
}

Schema.Types = {
  ObjectId: String,
};

/**
 *
 * @param {function} func schema create function
 */
const getTypeFromSchema = (func, excludeProps) => {
  const schema = func(Schema);
  return omit({
    _id: { type: String },
    ...schema.definitions,
  }, excludeProps);
};

const parseMongoQueryProp = (definitions, name, value) => {
  let path = name;
  let def = get(definitions, path);
  if (!def && String(path).indexOf('.') > 0) {
    const [currentPath, ...newPath] = String(path).split('.');
    def = get(definitions, currentPath);
    path = newPath.join('.');
  } else if (!def) {
    def = definitions;
  }
  // primitives
  if (typeof def === 'function' && typeof value !== 'object') return def(value);
  // arrays
  if (typeof def === 'object' && def.length) return parseMongoQueryProp(def[0], path, value);
  // objects
  if (typeof def === 'object' && !def.length) {
    if (def.type) return parseMongoQueryProp(def.type, path, value);
    return parseMongoQueryProp(def, path, value);
  }
  return value;
};

const parseMongoQuery = (func, query) => {
  const schema = func(Schema);
  const result = {};
  forEach(query, (val, key) => {
    result[key] = parseMongoQueryProp(schema.definitions, key, val);
  });
  return result;
};

module.exports = {
  getTypeFromSchema,
  parseMongoQuery,
};
