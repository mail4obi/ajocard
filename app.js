/* eslint-disable no-console */
require('dotenv').config();
const backgroundJobHandler = require('background_job_handler');

const app = require('./appREST');
const seeder = require('./seeder');

// seed app
(async () => {
  try {
    const result = await seeder();
    console.log(result);
    backgroundJobHandler.start(__dirname);
  } catch (error) {
    console.log(error);
    // exit app if seeding fails
    process.exit(-1);
  }
})();

const ip = process.env.IP || 'localhost';
const port = process.env.PORT || 10010;

// begin listening for client requests
app.listen(port, () => {
  console.log('runnging on', ip, port);
});
