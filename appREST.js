const a127 = require('a127-magic');
const express = require('express');
const cors = require('cors');
const mung = require('express-mung');
const bodyParser = require('body-parser');
const { isNumber, isString } = require('lodash');

const { verify } = require('./api/controllers/auth');
const { filterProperties } = require('./api/data/utils');

const app = express();

app.use(cors());
app.options('*', cors());

app.use('/apidocs', express.static('apidoc'));

// body parsers
app.use(bodyParser.json());

// check user auth credentials
// and carry out any preprocessing
app.use(verify);

// middleware to access response body object
app.use(mung.json(
  (body, req, res) => {
    if (isNumber(body)) {
      return res.end(body);
    }
    if (isString(body)) {
      return res.end(body);
    }
    const filtered = filterProperties(body, () => true, ['password'], ['password']);
    // do something with body
    if (res.permission) {
      return res.permission.filter(filtered);
    }
    return filtered;
  },
));

// initialize a127 framework
a127.init((config) => {
  // include a127 middleware
  app.use(a127.middleware(config));

  // error handler to emit errors as a json string
  app.use((error, req, res, next) => {
    let err = error;
    if (typeof err !== 'object') {
      // If the object is not an Error, create a representation that appears to be
      err = {
        message: String(err), // Coerce to string
      };
    } else {
      // Ensure that err.message is enumerable (It is not by default)
      Object.defineProperty(err, 'message', { enumerable: true });
    }

    // Return a JSON representation of #/definitions/ErrorResponse
    res.set('Content-Type', 'application/json');
    res.end(JSON.stringify(err));
  });
});

module.exports = app; // for testing
