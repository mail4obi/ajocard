const User = require('./api/data/user');

const { ADMIN_EMAIL } = process.env;
const { ADMIN_PASSWORD } = process.env;

module.exports = async () => {
  // create admin user
  let admin = await User.getUserByEmail(ADMIN_EMAIL);
  if (!admin) {
    admin = await User.createUser(ADMIN_EMAIL, ADMIN_PASSWORD, {
      role: 'admin',
    });
  }
  return {
    admin,
  };
};
