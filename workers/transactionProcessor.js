/* eslint-disable no-await-in-loop */
/* eslint-disable no-constant-condition */
const Queue = require('bull');

const Transaction = require('../api/data/transaction');
const { delay } = require('../api/data/utils');
const { QUEUE_TYPES } = require('../api/data/queue');

const { APP_ID, EXTERNAL_REDIS_QUEUE } = require('../config');

const processTransaction = async (job) => {
  console.log(job.data);
  try {
    const tx = await Transaction.processTransaction(job.data.data);
    return Promise.resolve(tx);
  } catch (error) {
    console.log(error);
    return Promise.reject(new Error(error.message));
  }
};

const task = async () => {
  try {
    const queue1 = new Queue(`${APP_ID}:transaction-manager:${QUEUE_TYPES.NEW_TRANSACTION}`, EXTERNAL_REDIS_QUEUE);
    const queue2 = new Queue(`${APP_ID}:transaction-manager:${QUEUE_TYPES.TRANSACTION_UPDATE}`, EXTERNAL_REDIS_QUEUE);

    queue1.process(processTransaction);

    queue2.process(processTransaction);

    while (true) {
      const count = [await queue1.getWaitingCount(), await queue2.getWaitingCount()];
      console.log(`************** transaction processor worker ${count} *****************`);
      await delay(60000);
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  task,
};
