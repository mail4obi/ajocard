const Transfer = require('../data/transfer');
const Tools = require('../data/tools');

module.exports = {
  getTransfer,
  getTransfers,
  createTransfer,
};

/**
 * @api {get} /transfers/:id Get transfer by id
 * @apiName GetTransfer
 * @apiGroup Transfer
 *
 * @apiUse Authentication
 * @apiParam {String} id id of transfer to retrieve
 * @apiUse PopulateQueryParam
 * @apiUse ModelTransfer
 */
async function getTransfer(req, res) {
  try {
    const id = req.swagger.params.id.value;

    const anyPermission = req.access.can(req.role).readAny('transfer');
    const ownPermission = req.access.can(req.role).readOwn('transfer');

    if (anyPermission.granted) {
      const transfer = await Transfer.getTransfer(id, req.query);
      res.permission = anyPermission;
      return res.json(transfer);
    }

    if (ownPermission.granted) {
      const transfer = await Transfer.getTransfer({ _id: id, user: req.user.id }, req.query);
      if (!transfer) return res.status(404).json({ message: 'transfer not found' });
      res.permission = ownPermission;
      return res.json(transfer);
    }

    return res.status(403).json({ message: 'operation not allowed' });
  } catch (error) {
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to fetch transfer' });
  }
}

/**
 * @api {get} /transfers Get all transfers
 * @apiName GetTransfers
 * @apiGroup Transfer
 *
 * @apiUse Authentication
 * @apiUse ListQueryParams
 * @apiUse ModelQueryTransfer
 * @apiUse OtherModelParams
 * @apiUse ModelTransfers
 */
async function getTransfers(req, res) {
  try {
    const readAnyPermission = req.access.can(req.role).readAny('transfer');
    const ownPermission = req.access.can(req.role).readOwn('transfer');

    if (readAnyPermission.granted) {
      const transfers = await Transfer.getTransfers(req.query);
      res.permission = readAnyPermission;
      return res.json(transfers);
    }

    if (ownPermission.granted) {
      const transfers = await Transfer.getTransfers({ ...req.query, user: req.user.id });
      res.permission = ownPermission;
      return res.json(transfers);
    }

    return res.status('403').json({ message: 'operation not allowed' });
  } catch (error) {
    console.log(error);
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to fetch transfers' });
  }
}

/**
 * @api {post} /transfers Create new transfer
 * @apiName CreateTransfer
 * @apiGroup Transfer
 *
 * @apiUse Authentication
 * @apiParam {String} receipientAgentId agent id
 * @apiParam {Number} amount amount to send
 * @apiParam {Number} OTP one time password
 * @apiParam {Number} pin user pin
 * @apiParam {String} [user] send user id
 * @apiUse ModelTransfer
 */
async function createTransfer(req, res) {
  try {
    const createAnyPermission = req.access.can(req.role).createAny('transfer');
    const createOwnPermission = req.access.can(req.role).createOwn('transfer');

    let data = {};

    if (createAnyPermission.granted) {
      data = createAnyPermission.filter(req.body);
      if (!req.body.user) {
        data.user = req.user.id;
      }
    } else if (createOwnPermission.granted) {
      data = createOwnPermission.filter({
        ...req.body, user: req.user.id,
      });
    } else {
      return res.status(403).json({ message: 'operation not allowed' });
    }

    // check OTP
    if (!await Tools.validateOTP(req.user.id, req.body.OTP)) {
      return res.status(403).json({ message: 'incorrect OTP supplied' });
    }

    // check PIN
    if (req.body.pin !== req.user.pin) {
      return res.status(403).json({ message: 'incorrect PIN supplied' });
    }

    const transfer = await Transfer.createTransfer(data);
    return res.json(transfer);
  } catch (error) {
    console.log(error);
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to create transfer' });
  }
}
