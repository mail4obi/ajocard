const Wallet = require('../data/wallet');

module.exports = {
  getWallet,
  getWallets,
};

/**
 * @api {get} /wallets/:id Get wallet by id
 * @apiName GetWallet
 * @apiGroup Wallet
 *
 * @apiUse Authentication
 * @apiParam {String} id id of wallet to retrieve
 * @apiUse PopulateQueryParam
 * @apiUse ModelWallet
 */
async function getWallet(req, res) {
  try {
    const id = req.swagger.params.id.value;

    const anyPermission = req.access.can(req.role).readAny('wallet');
    const ownPermission = req.access.can(req.role).readOwn('wallet');

    if (anyPermission.granted) {
      const wallet = await Wallet.getWallet(id, req.query);
      res.permission = anyPermission;
      return res.json(wallet);
    }

    if (ownPermission.granted) {
      const wallet = await Wallet.getWallet({ _id: id, user: req.user.id }, req.query);
      if (!wallet) return res.status(404).json({ message: 'wallet not found' });
      res.permission = ownPermission;
      return res.json(wallet);
    }

    return res.status(403).json({ message: 'operation not allowed' });
  } catch (error) {
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to fetch wallet' });
  }
}

/**
 * @api {get} /wallets Get all wallets
 * @apiName GetWallets
 * @apiGroup Wallet
 *
 * @apiUse Authentication
 * @apiUse ListQueryParams
 * @apiUse ModelQueryWallet
 * @apiUse OtherModelParams
 * @apiUse ModelWallets
 */
async function getWallets(req, res) {
  try {
    const readAnyPermission = req.access.can(req.role).readAny('wallet');
    const ownPermission = req.access.can(req.role).readOwn('wallet');

    if (readAnyPermission.granted) {
      const wallets = await Wallet.getWallets(req.query);
      res.permission = readAnyPermission;
      return res.json(wallets);
    }

    if (ownPermission.granted) {
      const wallets = await Wallet.getWallets({ ...req.query, user: req.user.id });
      res.permission = ownPermission;
      return res.json(wallets);
    }

    return res.status('403').json({ message: 'operation not allowed' });
  } catch (error) {
    console.log(error);
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to fetch wallets' });
  }
}
