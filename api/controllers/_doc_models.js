/**
 * @apiDefine ModelQueryTransaction
 * @apiParam {String} [user] user(ObjectId) property
 * @apiParam {String} [referenceId] referenceId property
 * @apiParam {String} [referenceType] referenceType property
 * @apiParam {Number} [amount] amount property
 * @apiParam {String} [description] description property
 * @apiParam {String="CREDIT,DEBIT"} [type] type property
 * @apiParam {String="NEW,FAILED,CONFIRMED"} [status=NEW] status property
 * @apiParam {Boolean} [processed] processed property
 */

/**
 * @apiDefine ModelCreateTransaction
 * @apiParam {String} user user(ObjectId) property
 * @apiParam {String} referenceId referenceId property
 * @apiParam {String} referenceType referenceType property
 * @apiParam {Number} amount amount property
 * @apiParam {String} [description] description property
 * @apiParam {String="CREDIT,DEBIT"} type type property
 * @apiParam {String="NEW,FAILED,CONFIRMED"} [status=NEW] status property
 * @apiParam {Boolean} [processed] processed property
 */

/**
 * @apiDefine ModelUpdateTransaction
 * @apiParam {String} [user] user(ObjectId) property
 * @apiParam {String} [referenceId] referenceId property
 * @apiParam {String} [referenceType] referenceType property
 * @apiParam {Number} [amount] amount property
 * @apiParam {String} [description] description property
 * @apiParam {String="CREDIT,DEBIT"} [type] type property
 * @apiParam {String="NEW,FAILED,CONFIRMED"} [status=NEW] status property
 * @apiParam {Boolean} [processed] processed property
 */

/**
 * @apiDefine ModelTransaction
 * @apiSuccess {String} user user(ObjectId) property
 * @apiSuccess {String} referenceId referenceId property
 * @apiSuccess {String} referenceType referenceType property
 * @apiSuccess {Number} amount amount property
 * @apiSuccess {String} description description property
 * @apiSuccess {String="CREDIT,DEBIT"} type type property
 * @apiSuccess {String="NEW,FAILED,CONFIRMED"} status=NEW status property
 * @apiSuccess {Boolean} processed processed property
 */

/**
 * @apiDefine ModelTransactions
 * @apiSuccess {Object[]} transaction List of transaction
 * @apiSuccess {String} transaction.user transaction.user(ObjectId) property
 * @apiSuccess {String} transaction.referenceId transaction.referenceId property
 * @apiSuccess {String} transaction.referenceType transaction.referenceType property
 * @apiSuccess {Number} transaction.amount transaction.amount property
 * @apiSuccess {String} transaction.description transaction.description property
 * @apiSuccess {String="CREDIT,DEBIT"} transaction.type transaction.type property
 * @apiSuccess {String="NEW,FAILED,CONFIRMED"} transaction.status=NEW transaction.status property
 * @apiSuccess {Boolean} transaction.processed transaction.processed property
 */

/**
 * @apiDefine ModelQueryTransfer
 * @apiParam {String} [user] user(ObjectId) property
 * @apiParam {String} [receipientAgentId] receipientAgentId property
 * @apiParam {String} [transaction] transaction(ObjectId) property
 * @apiParam {Number} [amount] amount property
 * @apiParam {String} [message] message property
 * @apiParam {String="NEW,FAILED,COMPLETED"} [status=NEW] status property
 */

/**
 * @apiDefine ModelCreateTransfer
 * @apiParam {String} user user(ObjectId) property
 * @apiParam {String} receipientAgentId receipientAgentId property
 * @apiParam {String} [transaction] transaction(ObjectId) property
 * @apiParam {Number} amount amount property
 * @apiParam {String} [message] message property
 * @apiParam {String="NEW,FAILED,COMPLETED"} [status=NEW] status property
 */

/**
 * @apiDefine ModelUpdateTransfer
 * @apiParam {String} [user] user(ObjectId) property
 * @apiParam {String} [receipientAgentId] receipientAgentId property
 * @apiParam {String} [transaction] transaction(ObjectId) property
 * @apiParam {Number} [amount] amount property
 * @apiParam {String} [message] message property
 * @apiParam {String="NEW,FAILED,COMPLETED"} [status=NEW] status property
 */

/**
 * @apiDefine ModelTransfer
 * @apiSuccess {String} user user(ObjectId) property
 * @apiSuccess {String} receipientAgentId receipientAgentId property
 * @apiSuccess {String} transaction transaction(ObjectId) property
 * @apiSuccess {Number} amount amount property
 * @apiSuccess {String} message message property
 * @apiSuccess {String="NEW,FAILED,COMPLETED"} status=NEW status property
 */

/**
 * @apiDefine ModelTransfers
 * @apiSuccess {Object[]} transfer List of transfer
 * @apiSuccess {String} transfer.user transfer.user(ObjectId) property
 * @apiSuccess {String} transfer.receipientAgentId transfer.receipientAgentId property
 * @apiSuccess {String} transfer.transaction transfer.transaction(ObjectId) property
 * @apiSuccess {Number} transfer.amount transfer.amount property
 * @apiSuccess {String} transfer.message transfer.message property
 * @apiSuccess {String="NEW,FAILED,COMPLETED"} transfer.status=NEW transfer.status property
 */

/**
 * @apiDefine ModelQueryUser
 * @apiParam {String} [agentId] agentId property
 * @apiParam {String} [email] email property
 * @apiParam {Number} [pin=1234] pin property
 * @apiParam {String="user,admin"} [role=user] role property
 */

/**
 * @apiDefine ModelCreateUser
 * @apiParam {String} agentId agentId property
 * @apiParam {String} email email property
 * @apiParam {Number} pin=1234 pin property
 * @apiParam {String="user,admin"} [role=user] role property
 */

/**
 * @apiDefine ModelUpdateUser
 * @apiParam {String} [agentId] agentId property
 * @apiParam {String} [email] email property
 * @apiParam {Number} [pin=1234] pin property
 * @apiParam {String="user,admin"} [role=user] role property
 */

/**
 * @apiDefine ModelUser
 * @apiSuccess {String} agentId agentId property
 * @apiSuccess {String} email email property
 * @apiSuccess {Number} pin=1234 pin property
 * @apiSuccess {String="user,admin"} role=user role property
 */

/**
 * @apiDefine ModelUsers
 * @apiSuccess {Object[]} user List of user
 * @apiSuccess {String} user.agentId user.agentId property
 * @apiSuccess {String} user.email user.email property
 * @apiSuccess {Number} user.pin=1234 user.pin property
 * @apiSuccess {String="user,admin"} user.role=user user.role property
 */

/**
 * @apiDefine ModelQueryWallet
 * @apiParam {String} [user] user(ObjectId) property
 * @apiParam {Number} [available] available property
 * @apiParam {Number} [pending] pending property
 */

/**
 * @apiDefine ModelCreateWallet
 * @apiParam {String} user user(ObjectId) property
 * @apiParam {String[]} [latestTransactions] latestTransactions(ObjectId) property
 * @apiParam {Number} [available] available property
 * @apiParam {Number} [pending] pending property
 */

/**
 * @apiDefine ModelUpdateWallet
 * @apiParam {String} [user] user(ObjectId) property
 * @apiParam {String[]} [latestTransactions] latestTransactions(ObjectId) property
 * @apiParam {Number} [available] available property
 * @apiParam {Number} [pending] pending property
 */

/**
 * @apiDefine ModelWallet
 * @apiSuccess {String} user user(ObjectId) property
 * @apiSuccess {String[]} latestTransactions latestTransactions(ObjectId) property
 * @apiSuccess {Number} available available property
 * @apiSuccess {Number} pending pending property
 */

/**
 * @apiDefine ModelWallets
 * @apiSuccess {Object[]} wallet List of wallet
 * @apiSuccess {String} wallet.user wallet.user(ObjectId) property
 * @apiSuccess {String[]} wallet.latestTransactions wallet.latestTransactions(ObjectId) property
 * @apiSuccess {Number} wallet.available wallet.available property
 * @apiSuccess {Number} wallet.pending wallet.pending property
 */
