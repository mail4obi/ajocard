const { get } = require('lodash');
const jwt = require('jsonwebtoken');
const Accesscontrol = require('accesscontrol');

const User = require('../data/user');
const AccessList = require('../accesscontrol');

const { TOKEN_SECRET } = process.env;

module.exports = {
  login,
  verify,
};

async function verify(req, res, next) {
  try {
    console.log(req.method, req.path, req.headers.authorization);
    if (req.headers.authorization) {
      const auth = req.headers.authorization;
      const parts = auth.split(' ');
      const decoded = jwt.verify(parts[1], TOKEN_SECRET);
      console.log('decoded jwt =>', decoded);
      const user = await User.getUser(decoded.data);

      if (!user) throw new Error('user not found');

      req.role = user.role;
      req.user = user;
      req.access = new Accesscontrol(get(AccessList, user.role));
    } else {
      req.role = 'default';
      req.user = { id: '', role: 'default' };
      req.access = new Accesscontrol(AccessList.default);
    }
    // continue
    return next();
  } catch (error) {
    console.log(error);
    return res.status(401).json({ message: error.message || error });
  }
}

/**
 * @api {post} /login login with email and password
 * @apiName Login
 * @apiGroup Auth
 * @apiParam {String} email User email
 * @apiParam {String} password User's password
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "_token": "............."
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "message": "email or password incorrect"
 *     }
 */
async function login(req, res) {
  try {
    const permission = req.access.can(req.role).createOwn('login');
    const { email, password } = req.body;
    const user = await User.authenticate(email, password);
    const token = jwt.sign({
      data: user.id,
    }, TOKEN_SECRET);
    res.permission = permission;
    return res.json(Object.assign({ _token: token }, user._doc));
  } catch (error) {
    console.log(error);
    return res.status(401).json({ message: 'email or password incorrect' });
  }
}
