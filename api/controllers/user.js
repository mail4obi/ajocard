const User = require('../data/user');

module.exports = {
  getUser,
  getUsers,
  updateUser,
  signup,
};

/**
 * @api {get} /users/:id Get user by id
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiUse Authentication
 * @apiParam {String} id id of user to retrieve
 * @apiUse ModelUser
 */
async function getUser(req, res) {
  try {
    const id = req.swagger.params.id.value;

    const anyPermission = req.access.can(req.role).readAny('user');
    const ownPermission = req.access.can(req.role).readOwn('user');

    if (anyPermission.granted) {
      const user = await User.getUser(id, req.query);
      res.permission = anyPermission;
      res.json(user);
    } else if (ownPermission.granted && req.user.id === id) {
      const user = await User.getUser(id, req.query);
      res.permission = ownPermission;
      res.json(user);
    } else {
      res.status(403).json({ message: 'operation not allowed' });
    }
  } catch (error) {
    console.log(error);
    res.status(error.message ? 400 : 500).json({
      message: error.message || 'failed to fetch user',
    });
  }
}

/**
 * @api {get} /users Get All Users
 * @apiName GetUsers
 * @apiGroup User
 * @apiPermission admin
 *
 * @apiUse Authentication
 * @apiUse ListQueryParams
 * @apiUse ModelQueryUser
 * @apiUse OtherModelParams
 * @apiUse ModelUsers
 */
async function getUsers(req, res) {
  try {
    const anyPermission = req.access.can(req.role).readAny('user');
    const ownPermission = req.access.can(req.role).readOwn('user');

    if (anyPermission.granted) {
      const users = await User.getUsers(req.query);
      res.permission = anyPermission;
      res.json(users);
    } else if (ownPermission.granted) {
      const users = await User.getUsers(Object.assign({}, req.query, { _id: req.user.id }));
      res.permission = ownPermission;
      res.json(users);
    } else {
      res.status(403).json({ message: 'operation not allowed' });
    }
  } catch (error) {
    console.log(error);
    res.status(error.message ? 400 : 500).json({
      message: error.message || 'failed to fetch user',
    });
  }
}

/**
 * @api {post} /signup Signup New User
 * @apiName Signup
 * @apiGroup User
 *
 * @apiParam {String} email Unique email of new user
 * @apiParam {String} password User's password
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "_id": "............."
 *     }
 */
async function signup(req, res) {
  try {
    const {
      email, password,
    } = req.body;

    const user = await User.createUser(email, password, {});

    return res.json({ ...user._doc });
  } catch (error) {
    console.log(error);
    return res.status(error.message ? 400 : 500).json({
      message: error.message || 'Signup failed',
    });
  }
}

/**
 * @api {put} /users/:id Update User
 * @apiName UpdateUser
 * @apiGroup User
 *
 * @apiUse Authentication
 * @apiParam {String} id Unique user id
 * @apiUse ModelUpdateUser
 * @apiUse ModelUser
 */
async function updateUser(req, res) {
  try {
    const id = req.swagger.params.id.value;

    const anyPermission = req.access.can(req.role).updateAny('user');
    const ownPermission = req.access.can(req.role).updateOwn('user');

    if ((req.user.id !== id && !anyPermission.granted) || !ownPermission.granted) {
      return res.status(403).json({ message: 'operation not allowed' });
    }

    const permission = anyPermission.granted ? anyPermission : ownPermission;

    const update = await User.updateUser(id, permission.filter(req.body));

    const anyReadPermission = req.access.can(req.role).readAny('user');
    const ownReadPermission = req.access.can(req.role).readOwn('user');

    res.permission = anyReadPermission.granted ? anyReadPermission : ownReadPermission;
    return res.json(update);
  } catch (error) {
    console.log(error);
    return res.status(error.message ? 400 : 500).json({
      message: error.message || 'failed to update user',
    });
  }
}
