const Tools = require('../data/tools');
const Transaction = require('../data/transaction');

module.exports = {
  getOtp,
  fundAccount,
};

/**
 * @api {get} /tools/OTP Generate OTP for user
 * @apiName GetOtp
 * @apiGroup tools
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "OTP": 123456,
 *       "expiryInSeconds": 30
 *     }
 * @apiUse Authentication
 */
async function getOtp(req, res) {
  try {
    if (req.role === 'default') {
      return res.status(403).json({ message: 'operation not allowed' });
    }
    const data = await Tools.generateOTP(req.user.id);
    return res.json(data);
  } catch (error) {
    console.log(error);
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to generate OTP' });
  }
}

/**
 * @api {post} /tools/fundAccount Fund account
 * @apiName FundAccount
 * @apiGroup tools
 *
 * @apiUse Authentication
 * @apiParam {Number} amount amount to fund
 * @apiUse ModelTransaction
 */
async function fundAccount(req, res) {
  try {
    if (req.role === 'default') {
      return res.status(403).json({ message: 'operation not allowed' });
    }
    if (req.body.amount <= 0) throw new Error('invalid amount supplied');
    const tx = await Transaction.createTransaction({
      user: req.user.id,
      amount: req.body.amount,
      description: 'Free funding',
      referenceId: Date.now(),
      referenceType: 'free',
      type: 'CREDIT',
      status: 'CONFIRMED',
    });
    return res.json(tx);
  } catch (error) {
    console.log(error);
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to fund account' });
  }
}
