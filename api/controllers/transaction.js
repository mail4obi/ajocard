const Transaction = require('../data/transaction');

module.exports = {
  getTransaction,
  getTransactions,
};

/**
 * @api {get} /transactions/:id Get transaction by id
 * @apiName GetTransaction
 * @apiGroup Transaction
 *
 * @apiUse Authentication
 * @apiParam {String} id id of transaction to retrieve
 * @apiUse PopulateQueryParam
 * @apiUse ModelTransaction
 */
async function getTransaction(req, res) {
  try {
    const id = req.swagger.params.id.value;

    const anyPermission = req.access.can(req.role).readAny('transaction');
    const ownPermission = req.access.can(req.role).readOwn('transaction');

    if (anyPermission.granted) {
      const transaction = await Transaction.getTransaction(id, req.query);
      res.permission = anyPermission;
      return res.json(transaction);
    }

    if (ownPermission.granted) {
      const transaction = await Transaction.getTransaction({
        _id: id, user: req.user.id,
      }, req.query);
      if (!transaction) return res.status(404).json({ message: 'transaction not found' });
      res.permission = ownPermission;
      return res.json(transaction);
    }

    return res.status(403).json({ message: 'operation not allowed' });
  } catch (error) {
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to fetch transaction' });
  }
}

/**
 * @api {get} /transactions Get all transactions
 * @apiName GetTransactions
 * @apiGroup Transaction
 *
 * @apiUse Authentication
 * @apiUse ListQueryParams
 * @apiUse ModelQueryTransaction
 * @apiUse OtherModelParams
 * @apiUse ModelTransactions
 */
async function getTransactions(req, res) {
  try {
    const readAnyPermission = req.access.can(req.role).readAny('transaction');
    const ownPermission = req.access.can(req.role).readOwn('transaction');

    if (readAnyPermission.granted) {
      const transactions = await Transaction.getTransactions(req.query);
      res.permission = readAnyPermission;
      return res.json(transactions);
    }

    if (ownPermission.granted) {
      const transactions = await Transaction.getTransactions({
        ...req.query, user: req.user.id,
      });
      res.permission = ownPermission;
      return res.json(transactions);
    }

    return res.status('403').json({ message: 'operation not allowed' });
  } catch (error) {
    console.log(error);
    return res.status(error.message ? 400 : 500).json({ message: error.message || 'failed to fetch transactions' });
  }
}
