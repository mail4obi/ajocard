const {
  set,
  omit,
  get,
  forEach,
  split,
  reduce,
  map,
  indexOf,
  first,
  isNumber,
  isString,
  isArray,
  isObject,
  replace,
  keys,
  filter,
  find,
  flattenDeep,
  uniq,
} = require('lodash');
const {
  DocumentQuery,
  Types,
} = require('mongoose');
const DateFn = require('date-fns');
const EJSON = require('mongodb-extended-json');

const PopulateData = require('./populateData');
const PopulatePropResolver = require('./populatePropResolver');

const getPopulateHandler = (populate = '') => {
  const parts = populate.split(',');
  const ignore = [];
  const accept = [];
  const overrides = keys(PopulateData);

  forEach(parts, (p) => {
    const parts2 = split(p, '.');
    if (overrides.indexOf(first(parts2)) >= 0) {
      ignore.push(p);
    } else {
      accept.push(p);
    }
  });

  return [
    accept.join(','),
    async (data) => {
      if (!data) return data;

      const promises = map(ignore, async (path) => {
        const parts3 = split(path, '.');
        const prop = first(parts3);
        let ids = [];
        if (isArray(data)) {
          ids = map(data, d => PopulatePropResolver(d, prop));
        } else {
          ids = [PopulatePropResolver(data, prop)];
        }
        ids = uniq(filter(ids, id => id !== 'undefined'));
        const populateResult = await PopulateData[prop](ids, parts3.slice(1).join('.'));
        return {
          path: prop,
          result: populateResult,
        };
      });

      let promiseResults = await Promise.all(promises);
      promiseResults = flattenDeep(promiseResults);

      if (isArray(data)) {
        return map(data, (d) => {
          const out = { ...d._doc, id: d.id };
          forEach(promiseResults, (result) => {
            const prop = result.path;
            out[prop] = find(result.result, r => r._id === String(d[prop]));
          });
          return out;
        });
      }
      const out = { ...data._doc, id: data.id };
      forEach(promiseResults, (result) => {
        const prop = result.path;
        out[prop] = find(result.result, r => r._id === String(data[prop]));
      });
      return out;
    },
  ];
};

const parseQueryData = (val) => {
  if (val && val.match && val.match(/\/.*\/.*/)) {
    // match for regex values
    try {
      eval(val);
      return eval(val);
    } catch (error) {
      // ignore error
    }
  } else if (
    val
    && val.match
    && (val.match(/\{.*\}/) || val.match(/(true|false)/))
  ) {
    // match for object values
    try {
      return EJSON.parse(val);
    } catch (error) {
      // ignore error
    }
  }

  // cast string to relevant ObjectId
  if (new RegExp('^[0-9a-fA-F]{24}$').test(val) && Types.ObjectId.isValid(val)) {
    return Types.ObjectId(val);
  }

  if (isObject(val) && !val.length) {
    const parsed = parseQuery(val);
    if (keys(parsed).length > 0) return parsed;
    return val;
  }

  if (isArray(val)) {
    return map(val, parseQueryData);
  }

  return val;
};

const parseQuery = (query) => {
  const data = omit(query,
    '_populate',
    '_select', '_count', '_or', '_and', '_sum', '_skip', '_limit', '_sort');

  forEach(data, (val, key) => {
    data[key] = parseQueryData(val);
  });

  return data;
};

const getSumData = (data, query) => {
  const regex = /(\w{1,})\[(.*)\]/g;
  const match = regex.exec(query);
  if (match && match.length > 1) {
    const parts = split(match[2], ',');
    switch (parts.length) {
      case 1:
        if (regex.test(parts[0])) {
          return getSumData(data, parts[0]);
        }
        return reduce(
          get(data, match[1]),
          (prev, curr) => prev + get(curr, parts[0], 0),
          0,
        );
      case 3:
        return reduce(get(data, match[1]), (prev, curr) => {
          if (get(curr, parts[0]) === parts[1]) {
            return prev + get(curr, parts[2], 0);
          }
          return prev;
        }, 0);
      default:
        return 0;
    }
  }
  return get(data, query, 0);
};

const filterProps = (data, condition, props, others) => {
  if (!data) {
    return data;
  }

  if (isNumber(data)) {
    return data;
  }

  if (isString(data)) {
    return data;
  }

  if (isArray(data)) {
    return filterArray(data, condition, props, others);
  }

  if (isObject(data)) {
    return filterObject(data, condition, props, others);
  }

  return data;
};

const filterArray = (data, condition, props, others) => map(
  data,
  d => filterProps(d, condition, props, others),
);

const filterObject = (data, condition, props, others) => {
  const dataClone = { ...data };
  if (condition(dataClone)) {
    forEach(dataClone, (val, key) => {
      if (indexOf(props, key) >= 0) {
        delete dataClone[key];
        return;
      }
      dataClone[key] = filterProps(val, condition, props, others);
    });
  } else {
    forEach(dataClone, (val, key) => {
      if (indexOf(others, key) >= 0) {
        delete dataClone[key];
        return;
      }
      dataClone[key] = filterProps(val, condition, props, others);
    });
  }
  return dataClone;
};

const getPopulateObject = (path) => {
  const parts = split(path, '.');
  const obj = {};
  let currentPath = 'populate';
  forEach(parts, (p) => {
    set(obj, `${currentPath}.path`, replace(p, ':', '.'));
    currentPath += '.populate';
  });
  return obj.populate;
};

module.exports = {
  getPopulateObject,
  parseQuery,
  generateGetSingleQuery: async (model, cond = {}, options = {}) => {
    let search;
    const populateTask = getPopulateHandler(options._populate);
    switch (typeof cond) {
      case 'string':
        search = model.findById(cond);
        break;
      default:
        search = model.findOne(cond);
    }
    if (options && options._select) {
      search.select(replace(options._select, /,/g, ' '));
    }
    if (options && options._populate) {
      try {
        const parts = populateTask[0].split(',');
        for (let i = 0; i < parts.length; i++) {
          search.populate(getPopulateObject(parts[i]));
        }
      } catch (error) {
        // no op
      }
    }
    const result = await search.exec();
    if (options && options._populate) {
      return await populateTask[1](result);
    }
    return result;
  },
  generateSearchQuery: async (model, cond = {}) => {
    const data = parseQuery(cond);
    /**
     * @type {DocumentQuery}
     */
    const search = model.find(data);
    const populateTask = getPopulateHandler(cond._populate);

    if (cond._or) {
      let or = [];
      try {
        or = JSON.parse(cond._or);
      } catch (error) {
        or = cond._or;
      }
      forEach(or, (val, key) => {
        or[key] = parseQuery(val);
      });
      search.or(or);
    }
    if (cond._and) {
      let and = [];
      try {
        and = JSON.parse(cond._and);
      } catch (error) {
        and = cond._and;
      }
      forEach(and, (val, key) => {
        and[key] = parseQuery(val);
      });
      search.and(and);
    }
    if (cond._sort) {
      const sort = {};
      try {
        const sortString = cond._sort.split(',');
        forEach(sortString, (s) => {
          const splitted = s.split(':');
          set(sort, splitted[0], splitted[1]);
        });
      } catch (error) {
        // no operation
      }
      search.sort(sort);
    }
    if (cond._skip) {
      search.skip(Number.parseInt(cond._skip));
    }
    if (cond._limit && Number.parseInt(cond._limit) <= 100) {
      search.limit(Number.parseInt(cond._limit));
    } else {
      search.limit(100);
    }
    if (cond._select) {
      search.select(replace(cond._select, /,/g, ' '));
    }
    if (cond._count) {
      return await search.countDocuments();
    }
    if (cond._sum) {
      const list = await search.exec();
      return list.reduce((prev, curr) => prev + Number(getSumData(curr, cond._sum)), 0);
    }
    if (cond._populate) {
      try {
        const parts = populateTask[0].split(',');
        for (let i = 0; i < parts.length; i++) {
          search.populate(getPopulateObject(parts[i]));
        }
      } catch (error) {
        // no op
      }
      const result = await search.exec();
      return await populateTask[1](result);
    }
    return search.exec();
  },
  filterProperties: (
    data,
    condition,
    props = ['password', 'isAdmin', 'documents'],
    others = ['password'],
  ) => {
    let filterData;
    try {
      if (data && data._doc) {
        filterData = data._doc;
      } else if (first(data) && first(data)._doc) {
        filterData = map(data, d => d._doc);
      } else {
        filterData = data;
      }
      return filterProps(
        JSON.parse(JSON.stringify(filterData)),
        condition,
        props,
        others,
      );
    } catch (error) {
      return filterData;
    }
  },
  /**
   * @param {Date} date
   * @param {Number} dayOfWeek
   * @return {Date}
   */
  getNextDayOfWeek: (date, dayOfWeek) => {
    // Code to check that date and dayOfWeek are valid left as an exercise ;)

    const resultDate = new Date(date.getTime());

    if (date.getDay() === dayOfWeek) {
      resultDate.setDate(date.getDate() + 7);
    }
    if (date.getDay() < dayOfWeek) {
      resultDate.setDate(date.getDate() + dayOfWeek);
    }
    if (date.getDay() > dayOfWeek) {
      resultDate.setDate(date.getDate() + 7 - dayOfWeek);
    }

    return resultDate;
  },
  /**
   * @param {String} startDate
   * @param {Number} value
   * @param {String} valueType
   * @param {Array<Number>} skipDays
   * @return {Date}
   */
  getNextDate: (startDate, value, valueType, skipDays) => {
    let time;
    switch (valueType) {
      case 'minute':
        time = DateFn.addMinutes(startDate, value);
        break;
      case 'hour':
        time = DateFn.addHours(startDate, value);
        break;
      case 'day':
        time = DateFn.addDays(startDate, value);
        break;
      case 'week':
        time = DateFn.addWeeks(startDate, value);
        break;
      case 'month':
        time = DateFn.addMonths(startDate, value);
        break;
      case 'year':
        time = DateFn.addYears(startDate, value);
        break;
      default:
      // nothing
    }
    let loop = 0;
    while (loop <= 7 && skipDays && skipDays.indexOf(time.getDay()) >= 0) {
      time = DateFn.addDays(time, 1);
      loop += 1;
    }
    return time;
  },
  delay: time => new Promise((resolve) => {
    setTimeout(() => {
      resolve(time);
    }, time);
  }),
};
