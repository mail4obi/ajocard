const { random } = require('lodash');

const Cache = require('./cache');

const generateOTP = async (userId) => {
  const OTP = random(900000, 999999);
  const expiry = 30;
  await Cache.setKey(`OTP-${userId}`, OTP, expiry);
  return { OTP, expiryInSeconds: expiry };
};

const validateOTP = async (userId, value) => {
  const storedOTP = await Cache.getKey(`OTP-${userId}`);
  return String(storedOTP) === String(value);
};

module.exports = {
  generateOTP,
  validateOTP,
};
