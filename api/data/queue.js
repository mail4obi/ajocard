const Queue = require('bull');

const { APP_ID, EXTERNAL_REDIS_QUEUE } = require('../../config');

const QUEUE_TYPES = {
  NEW_TRANSACTION: 'NEW_TRANSACTION',
  TRANSACTION_UPDATE: 'TRANSACTION_UPDATE',
  NEW_TRANSFER: 'NEW_TRANSFER',
  TRANSFER_UPDATE: 'TRANSFER_UPDATE',
};

const pushToQueue = async (type, data) => {
  const queue = new Queue(`${APP_ID}:transaction-manager:${type}`, EXTERNAL_REDIS_QUEUE);

  const options = {
    attempts: 10,
    backoff: {
      type: 'fixed',
      delay: 10000,
    },
    removeOnComplete: true,
  };
  await queue.add({ data }, options);
};

module.exports = {
  QUEUE_TYPES,
  pushToQueue,
};
