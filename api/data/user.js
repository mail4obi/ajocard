const bcrypt = require('bcrypt-nodejs');
const { keys } = require('lodash');
const { User } = require('../models');
const { generateSearchQuery, generateGetSingleQuery } = require('./utils');
const Cache = require('./cache');

const createUser = async (email, password, others) => {
  try {
    const existingEmail = await User.findOne({ email: new RegExp(email, 'i') });
    if (existingEmail) {
      throw new Error('email already exists');
    }
    const date = new Date();
    const dateString = `${date.getFullYear()}${String(date.getMonth()).padStart(2, 0)}${String(date.getDate()).padStart(2, 0)}`;
    const dayCount = await Cache.incrKey(`new-account-${dateString}`, 24 * 60 * 60);
    const agentId = `${dateString}${String(dayCount).padStart(4, 0)}`;
    const user = new User(Object.assign({
      email, password: bcrypt.hashSync(password), agentId,
    }, others));
    return await user.save();
  } catch (error) {
    throw error;
  }
};

const getUsers = async (cond) => {
  try {
    return await generateSearchQuery(User, cond);
  } catch (error) {
    throw error;
  }
};

const getUser = async (cond, options) => {
  try {
    return await generateGetSingleQuery(User, cond, options);
  } catch (error) {
    throw error;
  }
};

const getUserByEmail = async (email) => {
  try {
    return await User.findOne({ email: new RegExp(email, 'i') });
  } catch (error) {
    throw error;
  }
};

const authenticate = async (email, password) => {
  try {
    const user = await User.findOne({ email: new RegExp(email, 'i') });
    if (user && bcrypt.compareSync(password, user.password)) {
      return user;
    }
    throw new Error('email or password incorrect');
  } catch (error) {
    throw error;
  }
};

const updateUser = async (id, data) => {
  try {
    const user = await User.findById(id);
    keys(data).forEach((k) => {
      switch (k) {
        case 'password':
          user.password = bcrypt.hashSync(data[k]);
          break;
        default:
          user[k] = data[k];
      }
    });
    await user.save();
    return await User.findById(id);
  } catch (error) {
    throw error;
  }
};

module.exports = {
  createUser,
  getUsers,
  getUser,
  getUserByEmail,
  authenticate,
  updateUser,
};
