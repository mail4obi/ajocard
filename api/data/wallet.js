const { Wallet } = require('../models');
const { generateSearchQuery, generateGetSingleQuery } = require('./utils');

const createWallet = async (data) => {
  try {
    const wallet = new Wallet(data);
    return await wallet.save();
  } catch (error) {
    throw error;
  }
};

const getWallets = async (cond) => {
  try {
    return await generateSearchQuery(Wallet, cond);
  } catch (error) {
    throw error;
  }
};

/**
 * Finds a single wallet
 * @param {String|Object} cond wallet id or query
 * @param {Object} options
 */
const getWallet = async (cond, options) => {
  try {
    return await generateGetSingleQuery(Wallet, cond, options);
  } catch (error) {
    throw error;
  }
};

/**
 * Updates a single wallet
 * @param {String|Object} cond wallet id or query
 */
const updateWallet = async (cond, data) => {
  try {
    const update = Object.assign({}, data, { updatedAt: new Date() });
    switch (typeof cond) {
      case 'string':
        return await Wallet.findByIdAndUpdate(cond, update, { new: true });
      default:
        return await Wallet.updateMany(cond, update);
    }
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getWallet,
  getWallets,
  createWallet,
  updateWallet,
};
