const Redis = require('ioredis');

const { APP_ID, LOCAL_REDIS_SERVER } = require('../../config');

const localRedis = new Redis(LOCAL_REDIS_SERVER);

const setKey = async (key, value, expire) => {
  const ckey = `${APP_ID}:${key}`;
  if (expire) await localRedis.set(ckey, value, 'ex', expire);
  else await localRedis.set(ckey, value);
};

const getKey = async (key) => {
  const ckey = `${APP_ID}:${key}`;
  const data = await localRedis.get(ckey);
  return data;
};

const incrKey = async (key, expire) => {
  const value = await localRedis.incr(key);
  await localRedis.expire(key, expire);
  return value;
};

module.exports = {
  setKey,
  getKey,
  incrKey,
};
