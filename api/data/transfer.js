const { Transfer, User } = require('../models');
const { generateSearchQuery, generateGetSingleQuery } = require('./utils');
const Transaction = require('./transaction');
const { QUEUE_TYPES, pushToQueue } = require('./queue');

/**
 *
 * @param {{
 *  receipientAgentId: String,
 *  amount: Number
 * }} data
 */
const createTransfer = async (data) => {
  try {
    if (data.amount <= 0) throw new Error('invalid amount supplied');
    const agent = await User.findOne({ agentId: data.receipientAgentId });
    if (!agent) throw new Error('agent not found');
    let transfer = await new Transfer({
      user: data.user,
      receipientAgentId: data.receipientAgentId,
      amount: data.amount,
    }).save();
    try {
      // try to debit user
      const debit = await Transaction.createTransaction({
        user: data.user,
        amount: data.amount,
        description: `Transfer of ${data.amount} to ${agent.email}`,
        referenceId: transfer.id,
        referenceType: 'transfer',
        type: 'DEBIT',
      });
      transfer = await Transfer.findByIdAndUpdate(transfer.id, { transaction: debit.id });
      // send to queue for further processing
      await pushToQueue(QUEUE_TYPES.NEW_TRANSFER, transfer.id);
    } catch (error) {
      transfer = await updateTransfer(transfer.id, {
        status: 'FAILED',
        message: error.message,
      });
      throw error;
    }
    return transfer;
  } catch (error) {
    throw error;
  }
};

const getTransfers = async (cond) => {
  try {
    return await generateSearchQuery(Transfer, cond);
  } catch (error) {
    throw error;
  }
};

/**
 * Finds a single transfer
 * @param {String|Object} cond transfer id or query
 * @param {Object} options
 */
const getTransfer = async (cond, options) => {
  try {
    return await generateGetSingleQuery(Transfer, cond, options);
  } catch (error) {
    throw error;
  }
};

/**
 * Updates a single transfer
 * @param {String|Object} cond transfer id or query
 */
const updateTransfer = async (cond, data) => {
  try {
    const update = Object.assign({}, data, { updatedAt: new Date() });
    let transfer;
    switch (typeof cond) {
      case 'string':
        transfer = await Transfer
          .findByIdAndUpdate(cond, update, { new: true });
        break;
      default:
        transfer = await Transfer.updateMany(cond, update);
    }
    if (transfer.id) {
      // send to queue for further processing
      await pushToQueue(QUEUE_TYPES.TRANSFER_UPDATE, transfer.id);
    } else {
      // loop through transfer and send to update queue
      // not needed right now
    }
  } catch (error) {
    throw error;
  }
};

const processTransfer = async (id) => {
  try {
    let transfer = await Transfer.findById(id).populate('user');
    const agent = await User.findOne({
      agentId: transfer.receipientAgentId,
    });
    if (transfer.status === 'FAILED') {
      // revert the debit if any
      await Transaction.updateTransaction({
        referenceId: transfer.id,
        referenceType: 'transfer',
      }, { status: 'FAILED' });
    } else {
      // credit the receipient
      await Transaction.createTransaction({
        user: agent.id,
        amount: transfer.amount,
        description: `Transfer of ${transfer.amount} from ${transfer.user.email}`,
        referenceId: transfer.id,
        referenceType: 'transfer',
        type: 'CREDIT',
        status: 'CONFIRMED',
      });
      // update debit transaction
      await Transaction.updateTransaction(String(transfer.transaction), {
        status: 'CONFIRMED',
      });
      // mark transfer as completed
      transfer = await Transfer.findByIdAndUpdate(transfer.id, {
        status: 'COMPLETED',
      });
    }
    return transfer;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getTransfer,
  getTransfers,
  createTransfer,
  updateTransfer,
  processTransfer,
};
