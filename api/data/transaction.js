const { DB, Transaction, Wallet } = require('../models');
const { generateSearchQuery, generateGetSingleQuery } = require('./utils');
const { QUEUE_TYPES, pushToQueue } = require('./queue');

/**
 *
 * @param {{
 *  user: String,
 *  referenceId: String,
 *  referenceType: String,
 *  amount: String,
 *  description: String,
 *  type: String
 * }} data
 */
const createTransaction = async (data) => {
  try {
    const session = await DB.startSession();
    session.startTransaction();
    const walletQuery = {
      user: data.user,
    };
    await Wallet.createCollection();
    let wallet = await Wallet.findOne(walletQuery).session(session);
    if (!wallet) {
      [wallet] = await Wallet.create([walletQuery], { session });
    }
    if (data.type === 'DEBIT') {
      const cost = data.amount;
      if (wallet.available < cost) {
        throw new Error('not enough balance');
      }
      await Wallet.findByIdAndUpdate(wallet.id, {
        $inc: {
          available: -cost,
        },
      }).session(session);
    }
    if (data.type === 'CREDIT') {
      const cost = data.amount;
      await Wallet.findByIdAndUpdate(wallet.id, {
        $inc: {
          pending: cost,
        },
      }).session(session);
    }
    await Transaction.createCollection();
    const [transaction] = await Transaction.create([data], { session });
    await transaction.save();
    await session.commitTransaction();
    // send to queue for further processing
    await pushToQueue(QUEUE_TYPES.NEW_TRANSACTION, transaction.id);
    return transaction;
  } catch (error) {
    throw error;
  }
};

const getTransactions = async (cond) => {
  try {
    return await generateSearchQuery(Transaction, cond);
  } catch (error) {
    throw error;
  }
};

/**
 * Finds a single transaction
 * @param {String|Object} cond transaction id or query
 * @param {Object} options
 */
const getTransaction = async (cond, options) => {
  try {
    return await generateGetSingleQuery(Transaction, cond, options);
  } catch (error) {
    throw error;
  }
};

/**
 * Updates a single transaction
 * @param {String|Object} cond transaction id or query
 */
const updateTransaction = async (cond, data) => {
  try {
    const update = Object.assign({}, data, { updatedAt: new Date() });
    let transaction;
    switch (typeof cond) {
      case 'string':
        transaction = await Transaction
          .findByIdAndUpdate(cond, update, { new: true });
        break;
      default:
        transaction = await Transaction.updateMany(cond, update);
    }
    if (transaction.id) {
      // send to queue for further processing
      await pushToQueue(QUEUE_TYPES.TRANSACTION_UPDATE, transaction.id);
    } else {
      // loop through transaction and send to update queue
      // not needed right now
    }
    return transaction;
  } catch (error) {
    throw error;
  }
};

const processTransaction = async (id) => {
  try {
    const session = await DB.startSession();
    session.startTransaction();
    let transaction = await Transaction.findById(id).session(session);
    const wallet = await Wallet.findOne({
      user: transaction.user,
    }).session(session);
    // query for wallet that does not have current transaction
    const walletQuery = {
      _id: wallet.id,
      latestTransactions: { $ne: id },
    };
    // handle failed transactions
    if (transaction.status === 'FAILED') {
      // revert 1st leg
      const cost = transaction.amount;
      let update;
      switch (transaction.type) {
        case 'CREDIT':
          update = {
            $inc: {
              pending: -cost,
            },
            $push: { latestTransactions: id },
          };
          await Wallet.findOneAndUpdate(walletQuery, update).session(session);
          break;
        case 'DEBIT':
          update = {
            $inc: {
              available: cost,
            },
            $push: { latestTransactions: id },
          };
          await Wallet.findOneAndUpdate(walletQuery, update).session(session);
          break;
        default:
      }
    }
    // handle confirmed transactions
    if (transaction.status === 'CONFIRMED') {
      // finish 2nd leg
      const cost = transaction.amount;
      let update;
      switch (transaction.type) {
        case 'CREDIT':
          update = {
            $inc: {
              available: cost,
            },
            $push: { latestTransactions: id },
          };
          await Wallet.findOneAndUpdate(walletQuery, update).session(session);
          break;
        case 'DEBIT':
          update = {
            $inc: {
              pending: -cost,
            },
            $push: { latestTransactions: id },
          };
          await Wallet.findOneAndUpdate(walletQuery, update).session(session);
          break;
        default:
      }
    }
    if (['CONFIRMED', 'FAILED'].indexOf(transaction.status) >= 0) {
      transaction.processed = true;
    }
    if (wallet.latestTransactions.length > 50) {
      await Wallet.update(wallet.id, {
        $pop: { latestTransactions: 1 },
      });
    }
    transaction = await transaction.save();
    await session.commitTransaction();
    return transaction;
  } catch (error) {
    console.log(error);
    throw new Error(error.message);
  }
};

module.exports = {
  getTransaction,
  getTransactions,
  createTransaction,
  updateTransaction,
  processTransaction,
};
