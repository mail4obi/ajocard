module.exports = [
  {
    role: 'user', resource: 'transaction', action: 'read:own', attributes: '*',
  },

  {
    role: 'user', resource: 'transfer', action: 'read:own', attributes: '*',
  },
  {
    role: 'user', resource: 'transfer', action: 'create:own', attributes: '*',
  },

  {
    role: 'user', resource: 'user', action: 'read:own', attributes: '*',
  },
  {
    role: 'user', resource: 'user', action: 'create:own', attributes: '*,!role,!verified,!disabled',
  },
  {
    role: 'user', resource: 'user', action: 'update:own', attributes: '*,!email,!role,!verified,!disabled',
  },

  {
    role: 'user', resource: 'wallet', action: 'read:own', attributes: '*',
  },
];
