/* eslint-disable global-require */
const base = [
  { role: 'admin', resource: '_', action: 'read:any' },
  { role: 'default', resource: '_', action: 'read:any' },
];

module.exports = {
  admin: [...base, ...require('./admin')],
  user: [...base, ...require('./user')],
  default: [...base, ...require('./default')],
};
