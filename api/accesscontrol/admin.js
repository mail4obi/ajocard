module.exports = [
  {
    role: 'admin', resource: 'transaction', action: 'read:any', attributes: '*',
  },

  {
    role: 'admin', resource: 'transfer', action: 'read:any', attributes: '*',
  },
  {
    role: 'admin', resource: 'transfer', action: 'create:own', attributes: '*',
  },

  {
    role: 'admin', resource: 'user', action: 'read:any', attributes: '*',
  },
  {
    role: 'admin', resource: 'user', action: 'create:any', attributes: '*',
  },
  {
    role: 'admin', resource: 'user', action: 'update:any', attributes: '*',
  },

  {
    role: 'admin', resource: 'wallet', action: 'read:any', attributes: '*',
  },
];
