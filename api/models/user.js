module.exports = (Schema) => {
  const schema = new Schema({
    agentId: { type: String, unique: true, required: true },
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    pin: {
      type: Number,
      minLength: 4,
      maxLength: 4,
      required: true,
      default: 1234,
    },
    role: { type: String, default: 'user', enum: ['user', 'admin'] },
  }, { timestamps: true });

  schema.path('email').validate((email) => {
    const emailRegex = /^([\w-.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailRegex.test(email); // Assuming email has a text attribute
  }, 'invalid email');

  return schema;
};
