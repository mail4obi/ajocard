module.exports = (Schema) => {
  const schema = new Schema({
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    referenceId: { type: String, required: true, index: true },
    referenceType: { type: String, required: true, index: true },
    amount: { type: Number, required: true },
    description: { type: String },
    type: {
      type: String,
      enum: ['CREDIT', 'DEBIT'],
      index: true,
      required: true,
    },
    status: {
      type: String, enum: ['NEW', 'FAILED', 'CONFIRMED'], default: 'NEW', index: true,
    },
    processed: { type: Boolean, default: false },
  }, { timestamps: true });

  schema.index({ referenceId: 1, referenceType: 1, type: 1 }, { unique: true });

  return schema;
};
