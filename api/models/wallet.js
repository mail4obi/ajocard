module.exports = (Schema) => {
  const schema = new Schema({
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
      unique: true,
    },
    latestTransactions: [{
      type: Schema.Types.ObjectId, ref: 'Transaction',
    }],
    available: { type: Number, default: 0 },
    pending: { type: Number, default: 0 },
  }, { timestamps: true });

  return schema;
};
