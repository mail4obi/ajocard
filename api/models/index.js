const mongoose = require('mongoose');

const host = process.env.DB_HOST;
const port = process.env.DB_PORT;
const user = process.env.DB_USER;
const pass = process.env.DB_PASSWORD;
const dbname = process.env.DB_NAME;
const uri = process.env.DB_URI;
const poolSize = process.env.MONGO_POOL_SIZE || 5;

mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.plugin((schema) => {
  schema.pre('findOneAndUpdate', setRunValidators);
  schema.pre('findByIdAndUpdate', setRunValidators);
  schema.pre('updateMany', setRunValidators);
  schema.pre('updateOne', setRunValidators);
  schema.pre('update', setRunValidators);
});

function setRunValidators() {
  this.setOptions({ runValidators: true });
}

const { Schema } = mongoose;
if (uri) {
  mongoose
    .connect(uri, { useNewUrlParser: true, poolSize })
    .then(() => {
      console.log('connection established');
    })
    .catch((err) => {
      console.log(err);
    });
} else {
  mongoose
    .connect(`mongodb://${host}:${port}/${dbname}`, {
      user,
      pass,
      poolSize,
    })
    .then(() => {
      console.log('connection established');
    })
    .catch((err) => {
      console.log(err);
    });
}

const Transaction = mongoose.model('Transaction', require('./transaction')(Schema));
const Transfer = mongoose.model('Transfer', require('./transfer')(Schema));
const User = mongoose.model('User', require('./user')(Schema));
const Wallet = mongoose.model('Wallet', require('./wallet')(Schema));

module.exports = {
  Transaction,
  Transfer,
  User,
  Wallet,
  DB: mongoose,
};
