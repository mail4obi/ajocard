module.exports = (Schema) => {
  const schema = new Schema({
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    receipientAgentId: { type: String, required: true },
    transaction: { type: Schema.Types.ObjectId, ref: 'Transaction' },
    amount: { type: Number, required: true },
    message: { type: String },
    status: { type: String, enum: ['NEW', 'FAILED', 'COMPLETED'], default: 'NEW' },
  }, { timestamps: true });

  return schema;
};
