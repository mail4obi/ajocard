## Ajocard Test Service
This api uses swagger for route implementation and documentation

### Requirements
- nodejs and npm
- mongodb connection URI
- redis connection URI

### Installation
Refer to example.env for the necessary environment variables
```sh
npm install
```

### startup
```sh
npm start
```

### Documentation
The app has an additional documentation located in apidoc folder on route '/apidocs'.
start the app then Goto http://localhost:10010/apidocs/ to view api documentation.
A working version is on heroku at [this url](https://obi-ajocard.herokuapp.com) and the docs at [this url](https://obi-ajocard.herokuapp.com/apidocs/)

Note: on login with /login, the api returns a jwt token along with the response. This token should be used for authenticating requests.

A typical usage flow would be
- signup with POST:/signup or login with POST:/login to obtain a jwt token
- carry out a test account funding with POST:/tools/fund
- generate OTP token for transfer with GET:/tools/OTP
- transfer funds from one agent to another with POST:/transfers
- view transaction logs with GET:/transactions
- view wallet balance with GET:/wallets